package com.example;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import io.github.cdimascio.dotenv.Dotenv;
import java.util.Properties;

public class KafkaPublish {

    public static void publishToKafka(String message) {
        Dotenv dotenv = Dotenv.configure().load();
        Properties props = new Properties();

        String brokerUrl = dotenv.get("KAFKA_BOOTSTRAP_URL");
        String kafkaTopic = dotenv.get("KAFKA_TOPIC");


        props.put("bootstrap.servers",brokerUrl); 
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        try (Producer<String, String> producer = new KafkaProducer<>(props)) {
            ProducerRecord<String, String> record = new ProducerRecord<>(kafkaTopic, message);
            producer.send(record);
            System.out.println("Published to Kafka: " + message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}