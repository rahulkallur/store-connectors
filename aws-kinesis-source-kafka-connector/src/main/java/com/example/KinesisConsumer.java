package com.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.github.cdimascio.dotenv.Dotenv;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.kinesis.KinesisClient;
import software.amazon.awssdk.services.kinesis.model.*;

public class KinesisConsumer {
    private static final Logger log = LoggerFactory.getLogger(KinesisConsumer.class);

    public static void main(String[] args) {
        Dotenv dotenv = Dotenv.configure().load();
        // String STREAM_NAME = dotenv.get("STREAM_NAME");
        String ACCESS_KEY = dotenv.get("ACCESS_KEY");
        String SECRET_KEY = dotenv.get("SECRET_KEY");
        String REGION_NAME = dotenv.get("REGION_NAME");
        KinesisClient kinesisClient = KinesisClient.builder()
                .region(Region.of(REGION_NAME))  // Set your desired region
                .credentialsProvider(() -> AwsBasicCredentials.create(ACCESS_KEY, SECRET_KEY))
                .build();
        
                log.info("Starting Kinesis consumer...");

        String shardIterator = getShardIterator(kinesisClient);
        System.out.println("Kinesis consumer started...");
        // Continuously poll for new records
        System.out.println("Polling for new records...");
        while (true) {
            log.info("Polling for records...");
            GetRecordsRequest getRecordsRequest = GetRecordsRequest.builder()
                    .shardIterator(shardIterator)
                    .limit(10)
                    .build();
            GetRecordsResponse getRecordsResponse = kinesisClient.getRecords(getRecordsRequest);

            getRecordsResponse.records().forEach(record -> {
                String kafkaMessage = record.data().asUtf8String();

                // Publish the message to Kafka
                KafkaPublish.publishToKafka(kafkaMessage);
            });

            // Update shard iterator for the next iteration
            shardIterator = getRecordsResponse.nextShardIterator();

            // Introduce a delay to control the rate of polling
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                log.error("Error during sleep", e);
                e.printStackTrace();
            }
        }
    }

    private static String getShardIterator(KinesisClient kinesisClient) {
        Dotenv dotenv = Dotenv.configure().load();
        String STREAM_NAME = dotenv.get("STREAM_NAME");
        DescribeStreamRequest describeStreamRequest = DescribeStreamRequest.builder()
                .streamName(STREAM_NAME)
                .build();
        ShardIteratorType shardIteratorType = ShardIteratorType.LATEST;
        DescribeStreamResponse describeStreamResponse = kinesisClient.describeStream(describeStreamRequest);
        Shard shard = describeStreamResponse.streamDescription().shards().get(0);
        GetShardIteratorRequest getShardIteratorRequest = GetShardIteratorRequest.builder()
                .streamName(STREAM_NAME)
                .shardId(shard.shardId())
                .shardIteratorType(shardIteratorType)
                .build();
        GetShardIteratorResponse getShardIteratorResponse = kinesisClient.getShardIterator(getShardIteratorRequest);
        return getShardIteratorResponse.shardIterator();
    }
}