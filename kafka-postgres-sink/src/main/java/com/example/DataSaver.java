package com.example;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.text.SimpleDateFormat;
import java.util.Date;


public class DataSaver {
    public static void insertIntoDatabase(Connection connection, String data) throws SQLException {
        String insertQuery = "INSERT INTO tracking_history(uniqueId,ts,lat,lng,speed,internal_bat_voltage,external_bat_voltage,Direction,Event_Flag,isHA,isHB,GPS_Status,satellites,GSM_Signal,Fl_Level,pluscode,delta_distance,Distance) " + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        JsonObject jsonObject = JsonParser.parseString(data).getAsJsonObject();
        try (PreparedStatement preparedStatement = connection.prepareStatement(insertQuery)) {
            // Set parameters based on the data extracted from Kafka
            String uniqueId = jsonObject.get("uniqueId").getAsString();
            Double latitude = jsonObject.get("latitude").getAsDouble();
            Double longitude = jsonObject.get("longitude").getAsDouble();
            Integer timestamp = jsonObject.get("timestamp").getAsInt();
            Integer speed = jsonObject.get("speed").getAsInt();
            Integer intBatVol = jsonObject.has("intBatVol") ? jsonObject.get("intBatVol").getAsInt() : null;
            Integer extBatVol = jsonObject.has("extBatVol") ? jsonObject.get("extBatVol").getAsInt() : null;
            Integer direction = jsonObject.has("direction") ? jsonObject.get("direction").getAsInt() : null;
            Integer event_flag = jsonObject.has("event_flag") ? jsonObject.get("event_flag").getAsInt() : null;
            Boolean isHa = jsonObject.has("isHA") ? jsonObject.get("isHA").getAsBoolean() : null;
            Boolean isHb = jsonObject.has("isHB") ? jsonObject.get("isHB").getAsBoolean() : null;
            String gpsStatus = jsonObject.has("gpsStatus") ? jsonObject.get("gpsStatus").getAsString() : null;
            Integer satellites = jsonObject.has("satellites") ? jsonObject.get("satellites").getAsInt() : null;
            Integer gsmSignal = jsonObject.has("gpsSignal") ? jsonObject.get("gpsSignal").getAsInt() : null;
            Double fl_level = jsonObject.has("fl_level") ? jsonObject.get("fl_level").getAsDouble() : null;
            String pluscode = jsonObject.has("plusCode") ? jsonObject.get("plusCode").getAsString() : null;
            Float delta_distance = jsonObject.has("delta_distance") ? jsonObject.get("delta_distance").getAsFloat() : null;
            Float distance = jsonObject.has("distance") ? jsonObject.get("distance").getAsFloat() : null;

            ZoneId utcZone = ZoneId.of("UTC");
            Instant instant = Instant.ofEpochSecond(timestamp);
            ZoneId zoneId = ZoneId.systemDefault(); // You can specify a specific time zone if needed
            LocalDateTime dateTime = LocalDateTime.ofInstant(instant, zoneId);

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(utcZone);
            String formattedDateTime = instant.atZone(utcZone).format(formatter);
            // Set other parameters as needed
            System.out.println("Date Time: " + formattedDateTime);

            preparedStatement.setString(1,uniqueId);
            preparedStatement.setInt(2,timestamp);
            preparedStatement.setDouble(3,latitude);
            preparedStatement.setDouble(4,longitude);
            preparedStatement.setInt(5,speed);
            preparedStatement.setInt(6,intBatVol);
            preparedStatement.setInt(7,extBatVol);
            preparedStatement.setInt(8,direction);
            preparedStatement.setInt(9,event_flag);
            preparedStatement.setBoolean(10,isHa);
            preparedStatement.setBoolean(11,isHb);
            preparedStatement.setString(12,gpsStatus);
            preparedStatement.setInt(13,satellites);
            preparedStatement.setInt(14,gsmSignal);
            preparedStatement.setDouble(15,fl_level);
            preparedStatement.setString(16,pluscode);
            preparedStatement.setFloat(17,delta_distance);
            preparedStatement.setFloat(18,distance);

            // Execute the insert query
            preparedStatement.executeUpdate();
        }
    }
}