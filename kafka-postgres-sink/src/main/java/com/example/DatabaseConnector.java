package com.example;

import java.sql.*;
import java.sql.DriverManager;
import java.sql.SQLException;
import io.github.cdimascio.dotenv.Dotenv;


public class DatabaseConnector
{
    public static Connection connect() throws SQLException {
        Dotenv dotenv = Dotenv.configure().load();
        String url = dotenv.get("DB_URL");
        String user = dotenv.get("DB_USER");
        String password = dotenv.get("DB_PASSWORD");
        return DriverManager.getConnection(url, user, password);
    }
}