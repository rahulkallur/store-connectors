package com.example;

import java.sql.Connection;
import java.sql.SQLException;

public class Main {
    public static void main(String[] args) {
        System.out.println("-------------------------------------------------------------");
        System.out.println("-------------------------------------------------------------");
        System.out.println("Connecting to PostgreSQL Database...");
        try (Connection connection = DatabaseConnector.connect()) {
        System.out.println("Connected to PostgreSQL Database...");
        System.out.println("Connecting to Kafka Broker...");
            KafkaReader.readFromKafka();
        } catch (SQLException e) {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
