package com.example;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Properties;
import java.time.Duration;
import io.github.cdimascio.dotenv.Dotenv;


public class KafkaReader {
    public static void readFromKafka() {
        Dotenv dotenv = Dotenv.configure().load();
        Properties properties = new Properties();

        // Env variables
        String brokerUrl = dotenv.get("KAFKA_BOOTSTRAP_URL");
        String consumerGroup = dotenv.get("CONSUMER_GROUP");
        String kafkaTopic = dotenv.get("KAFKA_TOPIC");

        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, brokerUrl);
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, consumerGroup);
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        // Add other Kafka consumer configurations as needed

        Consumer<String, String> consumer = new KafkaConsumer<>(properties);
        consumer.subscribe(Collections.singletonList(kafkaTopic));

        System.out.println("Connected to Kafka Broker...");

        try (Connection connection = DatabaseConnector.connect()) {
            while (true) {
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));

                records.forEach(record -> {
                    // Process Kafka records and extract data for insertion
                    String data = record.value();

                    try {
                        // Call the insertIntoDatabase method for each Kafka record
                        DataSaver.insertIntoDatabase(connection, data);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                });
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}