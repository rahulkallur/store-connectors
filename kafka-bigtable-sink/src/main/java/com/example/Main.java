package com.example;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import io.github.cdimascio.dotenv.Dotenv;
import com.google.cloud.bigtable.data.v2.BigtableDataClient;


public class Main
{
    public static void main( String[] args )
    {
        Dotenv dotenv = Dotenv.configure().load();

        System.out.println( "Hello World!" );

        KafkaReader.readFromKafka();
    }
}
