package com.example;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.bigtable.data.v2.BigtableDataClient;
import com.google.cloud.bigtable.data.v2.BigtableDataSettings;
import io.github.cdimascio.dotenv.Dotenv;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class BigTableDataClient {

    public static BigtableDataClient createConnection() {
        try {
            Dotenv dotenv = Dotenv.configure().load();

            String projectId = dotenv.get("PROJECT_ID");
            String instanceId = dotenv.get("INSTANCE_ID");
            String serviceAccountKey = dotenv.get("SERVICE_ACCOUNT_CREDENTIAL");

            System.out.println("PROJECT ID: " + projectId + " INSTANCE ID: " + instanceId);

            byte[] decodedBytes = Base64.getDecoder().decode(serviceAccountKey);

            // Converting byte array to string
            String decodedString = new String(decodedBytes, StandardCharsets.UTF_8);
            // Convert JsonObject to a byte array
            byte[] bytes = decodedString.toString().getBytes(StandardCharsets.UTF_8);

            // Create a ByteArrayInputStream from the byte array
            ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);

            // Use GoogleCredentials to read from ByteArrayInputStream
            GoogleCredentials credentials = GoogleCredentials.fromStream(inputStream);

            // Set up BigtableDataSettings with credentials
            BigtableDataSettings settings = BigtableDataSettings.newBuilder()
                    .setProjectId(projectId)
                    .setInstanceId(instanceId)
                    .setCredentialsProvider(() -> credentials)
                    .build();

            // Create and return a new instance of BigtableDataClient
            return BigtableDataClient.create(settings);
        } catch (Exception e) {
            // Handle specific exceptions separately
            e.printStackTrace();
            return null;
        }
    }
}