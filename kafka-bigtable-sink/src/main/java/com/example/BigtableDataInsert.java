package com.example;

import com.google.cloud.bigtable.data.v2.models.Mutation;
import java.io.IOException;
import io.github.cdimascio.dotenv.Dotenv;
import com.google.cloud.bigtable.data.v2.BigtableDataClient;
import com.google.cloud.bigtable.data.v2.models.RowMutation;

public class BigtableDataInsert {

    public static void insertRecord(BigtableDataClient bigtableDataClient, String data) throws IOException {
        Dotenv dotenv = Dotenv.configure().load();
        // Specify the table ID
        String tableId = dotenv.get("TABLE_ID");

        // Specify the row key and column family
        String rowKey = "r1";
        String columnFamily = "cf1";

        // Specify the column qualifier
        String columnQualifier = "c1";

        // Build the mutation
        RowMutation rowMutation = RowMutation.create(tableId, rowKey)
                .setCell(columnFamily, columnQualifier, data);

        // Apply the mutation to Bigtable
        bigtableDataClient.mutateRow(rowMutation);

        System.out.println("Record inserted into Bigtable: " + data);
    }
}