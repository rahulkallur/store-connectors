package com.example;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import io.github.cdimascio.dotenv.Dotenv;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;
import com.google.cloud.bigtable.data.v2.BigtableDataClient;

public class KafkaReader {

    public static void readFromKafka() {
        Dotenv dotenv = Dotenv.configure().load();
        Properties properties = new Properties();

        // Env variables
        String brokerUrl = dotenv.get("KAFKA_BOOTSTRAP_URL");
        String consumerGroup = dotenv.get("CONSUMER_GROUP");
        String kafkaTopic = dotenv.get("KAFKA_TOPIC");

        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, brokerUrl);
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, consumerGroup);
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        // Add other Kafka consumer configurations as needed

        try (Consumer<String, String> consumer = new KafkaConsumer<>(properties);
             BigtableDataClient bigtableDataClient = BigTableDataClient.createConnection()) {

            consumer.subscribe(Collections.singletonList(kafkaTopic));
            System.out.println("Connected to Kafka Broker...");

            while (true) {
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));

                records.forEach(record -> {
                    // Process Kafka records and extract data for insertion
                    String data = record.value();

                    System.out.println("Record: " + data);

                    try {
                        // Call the insertIntoBigtable method for each Kafka record
                        BigtableDataInsert.insertRecord(bigtableDataClient, data);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
            }
        } catch (Exception e) {
            System.out.println("Error: " + e.toString());
        }
    }
}